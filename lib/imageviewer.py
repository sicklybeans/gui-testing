import argparse
import abc
import os
import sys
import logging
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk,GdkPixbuf

import abstractpixbuf
from abstractpixbuf import AbstractPixBuf

_DEFAULT_IMAGE_DIR = './images/large_collection'
_IMAGE_EXTENSIONS = ['.jpg', '.png', '.jpeg']
log = logging
_DEFAULT_LOG_LEVEL = 2 # warning
_LOG_ARG_TO_LEVEL = {
  0: logging.DEBUG,
  1: logging.INFO,
  2: logging.WARNING,
  3: logging.ERROR
}

class InvalidStateError(Exception):
  pass

class ImageIterator(abc.ABC):

  """Returns AbstractPixBuf instances."""

  @abc.abstractmethod
  def current(self): pass
  @abc.abstractmethod
  def next(self): pass
  @abc.abstractmethod
  def last(self): pass
  @abc.abstractmethod
  def has_next(self): pass
  @abc.abstractmethod
  def has_last(self): pass
  @abc.abstractmethod
  def peak_forward(self, steps): pass
  @abc.abstractmethod
  def peak_backward(self, steps): pass

class ImageIteratorPreCacher(object):

  def __init__(self, img_iterator, max_steps=10, cache_backward=False):
    self.itr = img_iterator
    self.max_steps = max_steps
    self.cache_backward = cache_backward

class FileFilter(abc.ABC):
  @abc.abstractmethod
  def accept(self, filename):
    pass

class ExtensionFilter(FileFilter):

  def __init__(self, extensions):
    self.extensions = [
      '.' + e if not e.startswith('.') else e
      for e in extensions
    ]

  def accept(self, filename):
    ext = os.path.splitext(filename)[1]
    return ext in self.extensions

def test_extension_filter():
  extensions = ['.png', 'jpg', '.jpeg']
  files = ['/an/image.png', 'image.jpg', 'image.jpeg', '/not/an/imagepng']
  exp = [files[0], files[1], files[2]]

  ef = ExtensionFilter(extensions)
  match = [f for f in files if ef.accept(f)]
  assert set(match) == set(exp)

class FolderIterator(ImageIterator):

  def __init__(self, folder, image_filter, use_recursive=False):
    self._pixbufs = []
    self._img_files = []
    self._img_map = {}
    self._index = 0
    self._filter = image_filter
    self._folder = folder
    self._use_recursive = use_recursive
    self._scan_images(self._folder)

  def _scan_images(self, folder):
    files = os.listdir(folder)
    for f in files:
      file = os.path.join(folder, f)
      if os.path.isdir(file) and not os.path.islink(file):
        if self._use_recursive:
          self._scan_images(file)
      if os.path.isfile(file):
        if self._filter.accept(file):
          self._pixbufs.append(AbstractPixBuf(os.path.abspath(file)))
          #self._img_files.append(os.path.abspath(file))

  def current(self):
    return self._pixbufs[self._index]
    # return self._load_image(self._index)

  def next(self):
    if not self.has_next():
      raise InvalidStateError('No next element')
    self._index += 1
    return self.current()

  def last(self):
    if not self.has_last():
      raise InvalidStateError('No last element')
    self._index -= 1
    return self.current()

  def has_current(self):
    return self._index >= 0 and self._index < len(self._pixbufs)

  def has_next(self):
    return self._index + 1 < len(self._pixbufs)

  def has_last(self):
    return self._index > 0

class ImageIteratorController(object):

  def __init__(self, image_iterator, image_window):
    self.iterator = image_iterator
    self.window = image_window

    self.window.connect("key-press-event", self.key_press)
    self.update_image()

  def key_press(self, window, ev):
    if ev.keyval == Gdk.KEY_Left:
      if self.iterator.has_last():
        self.iterator.last()
        self.update_image()
    if ev.keyval == Gdk.KEY_Right:
      if self.iterator.has_next():
        self.iterator.next()
        self.update_image()

  def update_image(self):
    #file = self.iterator.current()
    #image = Gtk.Image()
    #image.set_from_file(file)
    self.window.set_image(self.iterator.current())

class ImageWindow(Gtk.Window):

  def __init__(self):
    Gtk.Window.__init__(self, title="Hello World")
    self.connect("destroy", Gtk.main_quit)

    self.image = Gtk.Image()
    self.add(self.image)

  def set_image(self, pixbuf):
    if not instanceof(pixbuf, AbstractPixBuf):
      raise Exception('pixbuf must be an AbstractPixBuf instance')

    log.info('Setting image to "%s"' % str(pixbuf))

    size = (self.get_allocated_width(), self.get_allocated_height())
    self.image.set_from_pixbuf(pixbuf.get(size_limit=size))

  def set_size(self):
    raise Exception('Not implemented yet')

  def set_photo_sizing_behavior(self):
    raise Exception('Not implemented yet')

def log_test():
  log.basicConfig(level=logging.INFO)
  log.debug('Debug msg')
  log.info('Info msg')
  log.warning('Warning msg')
  log.error('Error msg')

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument(
    '--log', type=int, default=_DEFAULT_LOG_LEVEL,
    help='The logging level {0,1,2,3} = {debug, info, warning, error}',
    choices=_LOG_ARG_TO_LEVEL.keys())
  parser.add_argument(
    '--imgdir', type=str, default=_DEFAULT_IMAGE_DIR,
    help='The directory to scan for images')
  args = parser.parse_args(sys.argv[1:])

  log.basicConfig(level=_LOG_ARG_TO_LEVEL[args.log])

  win = ImageWindow()
  itr = FolderIterator(args.imgdir, ExtensionFilter(_IMAGE_EXTENSIONS))
  ctr = ImageIteratorController(itr, win)
  win.show_all()
  Gtk.main()


if __name__ == '__main__':
  main()
